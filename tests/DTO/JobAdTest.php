<?php

namespace App\Tests\DTO;

use App\DTO\JobAd;
use PHPUnit\Framework\TestCase;

class JobAdTest extends TestCase
{
    public function testSomething(): void
    {
        // best would be to use Faker https://fakerphp.github.io/ here, to test random values
        $data = [
                'id' => '1',
                'title' => 'Ingénieur Test Logiciel H/F',
                'link' => 'https://www.jj-tracker.com/fr/redirect/offer/non-existing-offer',
                'description' => 'description Ingenieur Test',
                'salary' => '1000$',
                'company' => 'Test Company',
                'city' => 'Bordeaux',
                'publicationDate' => '2023-06-04T02:00:26.000Z',
        ];

        $jobAd = new JobAd($data);

        self::assertSame($data['id'], $jobAd->id);
        self::assertSame($data['title'], $jobAd->title);
        self::assertSame($data['link'], $jobAd->link);
        self::assertSame($data['description'], $jobAd->description);
        self::assertSame($data['salary'], $jobAd->salary);
        self::assertSame($data['company'], $jobAd->company);
        self::assertSame($data['city'], $jobAd->city);
        self::assertSame(
            (new \DateTimeImmutable($data['publicationDate']))->getTimestamp(),
            $jobAd->publicationDate->getTimestamp()
        );
    }
}
