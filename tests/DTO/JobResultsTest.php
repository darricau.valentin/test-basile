<?php

namespace App\Tests\DTO;

use App\DTO\JobResults;
use PHPUnit\Framework\TestCase;

class JobResultsTest extends TestCase
{
    /**
     * @test
     */
    public function should_build_job_results_object(): void
    {
        $data = [
            'ads' => [
                [
                    'id' => '1',
                    'title' => 'Ingénieur Test Logiciel H/F',
                    'link' => 'https://www.jj-tracker.com/fr/redirect/offer/non-existing-offer',
                    'description' => 'description Ingenieur Test',
                    'salary' => '1000$',
                    'company' => 'Test Company',
                    'city' => 'Bordeaux',
                    'publicationDate' => '2023-06-04T02:00:26.000Z',
                ]
            ],
            'total' => 15,
        ];

        $jobResults = new JobResults($data, 20);

        self::assertSame(1, $jobResults->pageCount);
        self::assertSame(15, $jobResults->total);
        self::assertSame('1', $jobResults->jobAds[0]->id);
        self::assertSame('Bordeaux', $jobResults->jobAds[0]->city);
    }

    /**
     * @test
     */
    public function should_calculate_page_count(): void
    {
        $data = [
            'total' => 15,
            'ads' => [
                // ...
            ]
        ];

        $jobResults3Pages = new JobResults($data, 5);
        $jobResults4Pages = new JobResults($data, 4);

        self::assertSame(3, $jobResults3Pages->pageCount);
        self::assertSame(4, $jobResults4Pages->pageCount);
    }
}
