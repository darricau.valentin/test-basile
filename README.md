# Test Basile Valentin D

## Install

Requires docker, docker compose.

```bash
docker compose up -d
docker exec -it test-basile-php-valentin composer install
```

Access on http://127.0.0.1:52000/

## Tests

Run unit tests:

```bash
docker exec -it test-basile-php-valentin php bin/phpunit
```

Test I would add: test JobijobaApiJobRespository, with mock response.

## Note

Something weird happens when reaching page over 1111:

-   This fails (page=1112):
    https://api.jobijoba.com/v3/fr/ads/search?what=Test&where=Paris&page=1112&limit=9
-   This works (page=1111):
    https://api.jobijoba.com/v3/fr/ads/search?what=Test&where=Paris&page=1111&limit=9
