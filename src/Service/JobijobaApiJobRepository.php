<?php

namespace App\Service;

use App\Contract\JobRepositoryInterface;
use App\DTO\JobResults;
use App\Exception\LocationNotFoundException;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class JobijobaApiJobRepository implements JobRepositoryInterface
{
    private HttpClientInterface $client;

    private string|null $token = null;

    public function __construct(HttpClientInterface $client, private string $baseUri, private string $clientId, private string $clientSecret)
    {
        $this->client = $client->withOptions([
            'base_uri' => $baseUri,
        ]);
    }

    public function getJobs(string $what, string $where, int $page, int $limit): JobResults
    {
        $this->login();

        $response = $this->client->request('GET', 'ads/search', [
            'query' => [
                'what' => $what,
                'where' => $where,
                'page' => $page,
                'limit' => $limit,
            ],
            'headers' => [
                'Authorization' => 'Bearer ' . $this->token,
            ],
        ]);

        if ($response->getStatusCode() === 302) {
            throw new LocationNotFoundException($where, $response->getStatusCode());
        }

        return new JobResults($response->toArray()['data'], $limit);
    }

    private function login(): void
    {
        if ($this->token) {
            return;
        }

        $response = $this->client->request('POST', 'login', [
            'json' => [
                'client_id' => $this->clientId,
                'client_secret' => $this->clientSecret,
            ]
        ]);

        $this->token = $response->toArray()['token'];
    }
}
