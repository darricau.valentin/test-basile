<?php

namespace App\Form;

use App\DTO\JobSearch;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SearchType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class JobSearchType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->setMethod('GET')
            ->add('search', SearchType::class, [
                'label' => false,
                'attr' => [
                    'placeholder' => 'What job are you looking for?',
                    'minlength' => 2,
                    'maxlength' => 50,
                ],
            ])
            ->add('location', TextType::class, [ // This would autocomplete in a production environment
                'label' => false,
                'attr' => [
                    'placeholder' => 'Where?',
                    'minlength' => 2,
                    'maxlength' => 50,
                ],
            ])
            ->add('page', HiddenType::class)
            ->add('submit', SubmitType::class, [
                'label' => 'Search',
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => JobSearch::class,
        ]);
    }
}
