<?php

namespace App\DTO;

readonly class JobResults
{
    /** @var JobAd[] */
    public array $jobAds;
    public int $total;

    public int $pageCount;

    public function __construct(array $data, int $limit)
    {
        $this->total = $data['total'];
        $this->jobAds = array_map(fn(array $jobAd) => new JobAd($jobAd), $data['ads']);

        $this->pageCount = ceil($this->total / $limit);
    }
}