<?php

namespace App\DTO;

use DateTimeImmutable;

readonly class JobAd
{
    public string $id;
    public string $title;
    public string $link;
    public string $description;
    public string $salary;
    public string $company;
    public string $city;
    public DateTimeImmutable $publicationDate;

    public function __construct(array $data)
    {
        $this->id = $data['id'];
        $this->title = $data['title'];
        $this->link = $data['link'];
        $this->description = $data['description'];
        $this->salary = $data['salary'];
        $this->company = $data['company'];
        $this->city = $data['city'];
        $this->publicationDate = new DateTimeImmutable($data['publicationDate']);
    }
}