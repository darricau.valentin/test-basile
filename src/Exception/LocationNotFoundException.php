<?php

namespace App\Exception;

use RuntimeException;
use Throwable;

class LocationNotFoundException extends RuntimeException
{
    public function __construct(string $location, int $code = 0, Throwable $previous = null)
    {
        parent::__construct(sprintf('Location "%s" not found', $location), $code, $previous);
    }
}