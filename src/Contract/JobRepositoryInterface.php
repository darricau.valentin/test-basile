<?php

namespace App\Contract;
use App\DTO\JobResults;

interface JobRepositoryInterface
{
    public function getJobs(string $what, string $where, int $page, int $limit): JobResults;
}