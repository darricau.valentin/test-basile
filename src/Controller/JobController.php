<?php

namespace App\Controller;

use App\Contract\JobRepositoryInterface;
use App\DTO\JobSearch;
use App\Exception\LocationNotFoundException;
use App\Form\JobSearchType;
use Exception;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class JobController extends AbstractController
{
    private const LIMIT = 9;
    private const PAGES_AROUND = 3;

    #[Route('/', name: 'app_jobs')]
    public function index(JobRepositoryInterface $jobRepository, LoggerInterface $logger, Request $request): Response
    {
        $jobResults  = null;
        $pageCount = 0;
        $error = null;

        $jobSearch = new JobSearch();
        $jobSearch->setPage('1');

        $form = $this->createForm(JobSearchType::class, $jobSearch);
        $form->handleRequest($request);
        
        try {
            if ($form->isSubmitted() && $form->isValid()) {
                $jobSearch = $form->getData();
    
                $jobResults = $jobRepository->getJobs($jobSearch->getSearch(), $jobSearch->getLocation(), $jobSearch->getPage(), self::LIMIT);
            }
        } catch (LocationNotFoundException $exception) {
            $error = $exception->getMessage();
        } catch (Exception $exception) {
            $error = 'An error has occurred';
            $logger->critical($exception);
        }

        return $this->render('job/index.html.twig', [
            'form' => $form,
            'jobResults' => $jobResults,
            'limit' => self::LIMIT,
            'pagesAround' => self::PAGES_AROUND,
            'pageCount' => $pageCount,
            'error' => $error,
        ]);
    }
}
